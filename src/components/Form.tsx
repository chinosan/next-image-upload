'use client';
import React, { ChangeEvent, FormEvent, useState } from "react"
import Image from "next/image";

const Form = () => {
    const [currentFile, setCurrentFile] = useState<File | undefined>()
    const selectFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { files } = event.target;
        const selectedFiles = files as FileList;
        setCurrentFile(selectedFiles?.[0]);
      };

      const handleSubmit=async(e:FormEvent<HTMLFormElement>)=>{
        e.preventDefault();
        if(!currentFile) return;

        try {
          const data = new FormData();
          data.set('file', currentFile)

          const res = await fetch('/api/upload',{
            method: 'POST',
            body: data
          })
          console.log(res)
          if(res.ok){
            console.log('file uploaded')
          }
        } catch (error) {
          console.log(error)
        }
      }

      const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        if( !e.target.files?.[0]) return;
        setCurrentFile(e.target.files[0])
      }
  return (
    <div>
        <form onSubmit={handleSubmit}>
            <input type="file" name="file" id="" onChange={handleChange}  />
            <button type="submit" disabled={!currentFile}>Guardar</button>
        </form>
        {currentFile && (
          <Image
            src={URL.createObjectURL(currentFile)}
            alt="Uploaded file"
            className="w-64 h-64 object-contain mx-auto"
            width={256}
            height={256}
          />
        )}
    </div>
  )
}

export default Form